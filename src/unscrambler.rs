use eyre::{Result, WrapErr};
use format_sql_query::*;
use itertools::Itertools;
use tokio_postgres::{Client, NoTls};
use std::collections::HashMap;
use unicode_categories::UnicodeCategories;

pub struct Unscrambler {
    postgres_client: Client
}

impl Unscrambler {
    pub async fn new() -> Result<Unscrambler> {
        let (client, connection) = tokio_postgres::connect("host=localhost user=postgres dbname=unscrambler", NoTls).await
            .wrap_err("Could not initialize client")?;

        tokio::spawn(connection);

        client
            .batch_execute(
                "
            CREATE TABLE IF NOT EXISTS words (
                word             TEXT NOT NULL PRIMARY KEY,
                sorted_letters   TEXT NOT NULL,
                occurrences      INT
            );
            CREATE TABLE IF NOT EXISTS pairs (
                first            TEXT NOT NULL,
                first_sorted     TEXT NOT NULL,
                second           TEXT NOT NULL,
                second_sorted    TEXT NOT NULL,
                occurrences      INT,
                PRIMARY KEY(first, second)
            );
            CREATE INDEX IF NOT EXISTS letters ON words (sorted_letters);
            CREATE INDEX IF NOT EXISTS i1 ON pairs (first_sorted, second);
            CREATE INDEX IF NOT EXISTS i2 ON pairs (first, second_sorted);
        ",
            )
            .await.wrap_err("Could not insert tables")?;

        Ok(Unscrambler {
            postgres_client: client,
        })
    }


    pub async fn train_dictionary(&mut self, dictionary: &Vec<String>) -> Result<()> {
        let mut transaction = String::from("");
        for word in dictionary {
            let word = word.to_lowercase();
            transaction += &format!(
                "
                INSERT INTO words VALUES ({word}, {word_sorted}, 0) ON CONFLICT DO NOTHING;
                UPDATE words
                    SET occurrences = occurrences + 1
                WHERE word = {word};",
                word = QuotedData(&word),
                word_sorted = QuotedData(&word.chars().sorted().collect::<String>())
            );
        }
        self.postgres_client.batch_execute(&transaction).await?;
        Ok(())
    }

    pub async fn train_text(&mut self, text: &str) -> Result<()> {
        let word_split: Vec<&str> = text.split(" ").skip_while(|&x| x.is_empty()).collect();
        let mut sentences = Vec::<Vec<&str>>::new();
        let mut current_sentence = Vec::<&str>::new();
        for word in word_split {
            current_sentence.push(word);
            if word.contains(".") {
                sentences.push(current_sentence);
                current_sentence = Vec::new();
            }
        }
        sentences.push(current_sentence);
        for sentence in sentences {
            self.train_sentence(&sentence.join(" ")).await?;
        }
        Ok(())
    }

    pub async fn train_sentence(&mut self, text: &str) -> Result<()> {
        let text = text.to_lowercase();
        let text = text
            .chars()
            .filter(|x| x.is_letter() || x.is_number() || x == &' ' || x == &'-')
            .collect::<String>();
        let mut split: Vec<&str> = text.split(" ").skip_while(|&x| x.is_empty()).collect();
        split.push(".");
        let mut transaction = String::from("");
        for (i, word) in split.iter().enumerate() {
            transaction += &format!(
                "
                INSERT INTO words VALUES ({word}, {word_sorted}, 0) ON CONFLICT DO NOTHING;
                UPDATE words
                    SET occurrences = occurrences + 1
                WHERE word = {word};",
                word = QuotedData(&word),
                word_sorted = QuotedData(&word.chars().sorted().collect::<String>())
            );

            let previous_word: &str;
            if i == 0 {
                previous_word = ".";
            } else {
                previous_word = split[i - 1];
            }

            transaction += &format!("
                INSERT INTO pairs VALUES ({previous_word}, {previous_word_sorted}, {next_word}, {next_word_sorted}, 0) ON CONFLICT DO NOTHING;
                UPDATE pairs
                    SET occurrences = occurrences + 1
                WHERE first = {previous_word} AND second = {next_word};",
                previous_word=QuotedData(&previous_word),
                previous_word_sorted=QuotedData(&previous_word.chars().sorted().collect::<String>()),
                next_word=QuotedData(&word),
                next_word_sorted=QuotedData(&word.chars().sorted().collect::<String>()));
        }
        self.postgres_client.batch_execute(&transaction).await?;

        Ok(())
    }

    pub async fn unscramble_text(&mut self, text: &str) -> Result<String> {
        let word_split: Vec<&str> = text.split(" ").skip_while(|&x| x.is_empty()).collect();
        let mut sentences = Vec::<Vec<&str>>::new();
        let mut current_sentence = Vec::<&str>::new();
        for word in word_split {
            current_sentence.push(word);
            if word.contains(".") {
                sentences.push(current_sentence);
                current_sentence = Vec::new();
            }
        }
        sentences.push(current_sentence);
        let mut final_text = String::from("");
        for (_i, sentence) in sentences.iter().enumerate() {
            // println!("{}% done", (i as f64 / sentences.len() as f64) * 100.0);
            let unscrambled_text = self.unscramble_sentence(&sentence.join(" ")).await?;
            final_text += &(unscrambled_text + &" ");
        }
        Ok(final_text)
    }

    pub async fn unscramble_sentence(&mut self, text: &str) -> Result<String> {
        // Preprocess text
        let split: Vec<&str> = text.split(" ").skip_while(|&x| x.is_empty()).collect();
        let mut prepended_chars = vec![String::from(""); split.len()];
        for (i, word) in split.iter().enumerate() {
            for symbol in vec!["„"] {
                if word.contains(symbol) {
                    prepended_chars[i] = symbol.to_string();
                }
            }
        }
        let mut appended_chars = vec![String::from(""); split.len()];
        for (i, word) in split.iter().enumerate() {
            for symbol in vec![",", ":", ";", "!", ".", "“"] {
                if word.contains(symbol) {
                    appended_chars[i] = symbol.to_string();
                }
            }
        }
        let mut words_with_capitals = vec![None; split.len()];
        for (i, word) in split.iter().enumerate() {
            for symbol in word.chars() {
                if symbol.is_uppercase() {
                    if words_with_capitals[i].is_some() {
                        words_with_capitals[i] = None;
                        break;
                    } else {
                        words_with_capitals[i] = Some(symbol);
                    }
                }
            }
        }
        let split: Vec<String> = split
            .iter()
            .map(|text| {
                text.to_lowercase()
                    .chars()
                    .filter(|x| x.is_letter() || x.is_number() || x == &' ' || x == &'-')
                    .collect()
            })
            .collect();

        let mut sentence_candidates = vec![HashMap::new(); split.len()];

        for (i, scrambled_word) in split.clone().into_iter().enumerate() {
            let sorted_word = scrambled_word.chars().sorted().collect::<String>();
            let found;
            if let Some(capital) = words_with_capitals[i] {
                found = self.postgres_client.query(
                    "SELECT * FROM words WHERE sorted_letters = $1 AND SUBSTRING(word, 1, 1) = $2",
                    &[&sorted_word, &String::from(capital).to_lowercase()],
                ).await?;
            } else {
                found = self.postgres_client.query(
                    "SELECT * FROM words WHERE sorted_letters = $1",
                    &[&sorted_word],
                ).await?;
            }
            let mut candidates = HashMap::new();
            let mut total_occurences = 0.0;
            for row in found {
                let word_borrowed: &str = row.get(0);
                let word = String::from(word_borrowed);
                let occurences: i32 = row.get(2);
                candidates.insert(word, occurences as f64);
                total_occurences += occurences as f64;
            }
            for (_word, occurences) in candidates.iter_mut() {
                *occurences /= total_occurences;
            }
            sentence_candidates[i] = candidates;
        }

        // Update confidences

        let mut final_sentence = vec![None; split.len()];
        let mut updated_once = vec![false; split.len()];
        let mut last_changed_word_index: Option<usize> = None;
        let mut last_changed_word = String::from(".");

        for _ in 0..split.len() {
            // Update confidences with surrounding words. If last_changed_word_index is none, so in
            // the beginning of the loop, update first and last word confidences.
            // Otherwise, update confidences of words around the last changed word.
            //
            // target is if we want to change the precursor or the follower of the last word that
            // changed.
            enum Target {
                Precursor,
                Follower,
            }
            for target in vec![Target::Precursor, Target::Follower] {
                // found_surroundings is the sql object of the followers found
                let found_surroundings;
                // // total_surroundings is the number of total possibilities
                let total_surroundings;
                // surr_word_index is the index of the word of which we are updating the confidence
                let surr_word_index: usize;
                match target {
                    Target::Precursor => {
                        if last_changed_word_index.is_none() {
                            surr_word_index = split.len() - 1;
                        } else {
                            if last_changed_word_index.unwrap() == 0 {
                                continue;
                            }
                            surr_word_index = last_changed_word_index.unwrap() - 1;
                        }
                        if last_changed_word_index.is_some()
                            && words_with_capitals[last_changed_word_index.unwrap()].is_some()
                        {
                            let first_char_of_last_changed_word =
                                words_with_capitals[last_changed_word_index.unwrap()].unwrap();
                            found_surroundings = self.postgres_client.query(
                                "SELECT * FROM pairs WHERE first_sorted = $1 AND second = $2 AND SUBSTRING(second, 1, 1) = $3",
                                &[
                                    &split[surr_word_index].chars().sorted().collect::<String>(),
                                    &last_changed_word,
                                    &String::from(first_char_of_last_changed_word).to_lowercase()
                                ]
                            ).await?;
                            total_surroundings = self.postgres_client.query(
                                "SELECT sum(occurrences) FROM pairs WHERE first_sorted = $1 AND second = $2 AND SUBSTRING(second, 1, 1) = $3",
                                &[
                                    &split[surr_word_index].chars().sorted().collect::<String>(),
                                    &last_changed_word,
                                    &String::from(first_char_of_last_changed_word).to_lowercase()
                                ]
                            ).await?;
                        } else {
                            found_surroundings = self.postgres_client.query(
                                "SELECT * FROM pairs WHERE first_sorted = $1 AND second = $2",
                                &[
                                    &split[surr_word_index].chars().sorted().collect::<String>(),
                                    &last_changed_word,
                                ],
                            ).await?;
                            total_surroundings = self.postgres_client.query(
                                "SELECT sum(occurrences) FROM pairs WHERE first_sorted = $1 AND second = $2",
                                &[
                                    &split[surr_word_index].chars().sorted().collect::<String>(),
                                    &last_changed_word
                                ]
                            ).await?;
                        }
                    }
                    Target::Follower => {
                        if last_changed_word_index.is_none() {
                            surr_word_index = 0;
                        } else {
                            if last_changed_word_index.unwrap() == split.len() - 1 {
                                continue;
                            }
                            surr_word_index = last_changed_word_index.unwrap() + 1;
                        }
                        if last_changed_word_index.is_some()
                            && words_with_capitals[last_changed_word_index.unwrap()].is_some()
                        {
                            let first_char_of_last_changed_word =
                                words_with_capitals[last_changed_word_index.unwrap()].unwrap();
                            found_surroundings = self.postgres_client.query(
                                "SELECT * FROM pairs WHERE first = $1 AND second_sorted = $2 AND SUBSTRING(first, 1, 1) = $3",
                                &[
                                    &last_changed_word,
                                    &split[surr_word_index].chars().sorted().collect::<String>(),
                                    &String::from(first_char_of_last_changed_word).to_lowercase()
                                ]
                            ).await?;
                            total_surroundings = self.postgres_client.query(
                                "SELECT sum(occurrences) FROM pairs WHERE first = $1 AND second_sorted = $2 AND SUBSTRING(first, 1, 1) = $3",
                                &[
                                    &last_changed_word,
                                    &split[surr_word_index].chars().sorted().collect::<String>(),
                                    &String::from(first_char_of_last_changed_word).to_lowercase()
                                ]
                            ).await?;
                        } else {
                            found_surroundings = self.postgres_client.query(
                                "SELECT * FROM pairs WHERE first = $1 AND second_sorted = $2",
                                &[
                                    &last_changed_word,
                                    &split[surr_word_index].chars().sorted().collect::<String>(),
                                ],
                            ).await?;
                            total_surroundings = self.postgres_client.query(
                                "SELECT sum(occurrences) FROM pairs WHERE first = $1 AND second_sorted = $2",
                                &[
                                    &last_changed_word,
                                    &split[surr_word_index].chars().sorted().collect::<String>()
                                ]
                            ).await?;
                        }
                    }
                }
                let total_surroundings: Option<i64> = total_surroundings[0].get("sum");

                if total_surroundings.is_some() {
                    let total_surroundings = total_surroundings.unwrap();
                    let mut candidates = HashMap::new();
                    for row in found_surroundings {
                        let word_borrowed: &str;
                        match target {
                            Target::Follower => word_borrowed = row.get(2),
                            Target::Precursor => word_borrowed = row.get(0),
                        }
                        let word = String::from(word_borrowed);
                        let occurences: i32 = row.get(4);

                        candidates.insert(word, occurences as f64);
                    }

                    for (word, occurrences) in candidates {
                        if sentence_candidates[surr_word_index].contains_key(&word) {
                            if updated_once[surr_word_index] {
                                *sentence_candidates[surr_word_index].get_mut(&word).unwrap() =
                                    ((*sentence_candidates[surr_word_index]
                                        .get_mut(&word)
                                        .unwrap())
                                        + (occurrences / total_surroundings as f64))
                                        / 2.0;
                            } else {
                                *sentence_candidates[surr_word_index].get_mut(&word).unwrap() =
                                    occurrences / total_surroundings as f64;
                            }
                        } else {
                            sentence_candidates[surr_word_index]
                                .insert(word, occurrences / total_surroundings as f64);
                        }
                    }
                }
                updated_once[surr_word_index] = true;
            }

            // Determine word with max confidence
            let mut word_with_max_confidence = None;
            let mut word_with_max_confidence_index = 0;
            let mut max_confidence = -1.0;
            for (i, confidence_vec) in sentence_candidates.clone().into_iter().enumerate() {
                if confidence_vec.is_empty() && max_confidence < 0.0 && final_sentence[i] == None {
                    word_with_max_confidence = Some(String::from("[") + &split[i] + "]");
                    word_with_max_confidence_index = i;
                }
                for (dict_word, confidence) in confidence_vec {
                    if confidence > max_confidence && final_sentence[i] == None {
                        max_confidence = confidence;
                        word_with_max_confidence = Some(dict_word);
                        word_with_max_confidence_index = i;
                    }
                }
            }
            last_changed_word = word_with_max_confidence.unwrap();

            // If this word is a number, we shouldn't replace it and warn of this
            let mut number_chars = 0;
            for word_char in last_changed_word.chars() {
                if word_char.is_numeric() {
                    number_chars += 1;
                }
            }
            if number_chars >= 2 {
                final_sentence[word_with_max_confidence_index] = Some(last_changed_word.to_string() + "(?)");
            } else {
                final_sentence[word_with_max_confidence_index] = Some(last_changed_word.to_string());
            }

            last_changed_word_index = Some(word_with_max_confidence_index);
        }

        let mut final_sentence: Vec<String> = final_sentence
            .iter()
            .map(|x| String::from(x.as_ref().unwrap()))
            .collect();

        for (i, symbol) in prepended_chars.iter().enumerate() {
            final_sentence[i] = symbol.to_owned() + &final_sentence[i];
        }
        for (i, symbol) in appended_chars.iter().enumerate() {
            final_sentence[i] += symbol;
        }

        for (i, had_capital) in words_with_capitals.iter().enumerate() {
            if had_capital.is_some() {
                // uhhhh
                let mut v: Vec<char> = final_sentence[i].chars().collect();
                v[0] = v[0].to_uppercase().nth(0).unwrap();
                final_sentence[i] = v.into_iter().collect();
            }
        }

        Ok(final_sentence.join(" "))
    }
}
