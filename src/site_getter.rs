use soup::prelude::*;
use reqwest;
use eyre::{Result};

pub async fn get_text(url: &str) -> Result<Vec<String>> {
    let url = reqwest::Url::parse(url)?;
    let body = reqwest::get(url).await?.text().await?;
    let soup = Soup::new(&body);
    let intro = soup.class("park-article__intro").find();
    let intro_text;
    if let Some(intro) = intro {
        intro_text = intro.text();
    } else {
        intro_text = String::from("");
    }
    let scrambled_text = soup.class("text-blurred").find_all();
    let mut scrambled_text_vec = scrambled_text.map(|a| a.text().trim().to_string())
       .collect::<Vec<_>>();
    let mut final_vec = vec![intro_text];
    final_vec.append(&mut scrambled_text_vec);

    Ok(final_vec)
}
